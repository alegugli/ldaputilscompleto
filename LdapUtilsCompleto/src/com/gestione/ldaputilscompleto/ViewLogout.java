package com.gestione.ldaputilscompleto;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class ViewLogout extends VerticalLayout implements View {
	
	private static final long serialVersionUID = -9173491863820627128L;	
	private VerticalLayout vLayout;	
	private Label labelResponse;
	private String isAdmin;
	private boolean isLoaded;
	
	public ViewLogout()
	{				
		isAdmin = "";
		isLoaded = false;
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		if(isLoaded)
		{
			vLayout.removeAllComponents();
			removeComponent(vLayout);
		}
		
		vLayout = new VerticalLayout();
		vLayout.setMargin(true);
		if(VaadinSession.getCurrent().getAttribute("admin") != null)
			isAdmin = (String) VaadinSession.getCurrent().getAttribute("admin");
		if(isAdmin == "1")
		{											
			labelResponse = new Label();
			labelResponse.setContentMode(ContentMode.HTML);			
			
			VaadinSession.getCurrent().close();
						
			labelResponse.setValue("Logout effettuato.");
			vLayout.addComponent(labelResponse);
		}
		else
		{
			vLayout.addComponent(new Label("Accesso negato. E' necessaria l'<a href=\"/LdapUtilsCompleto/\">autenticazione</a>.", ContentMode.HTML));
		}
		addComponent(vLayout);
		isLoaded = true;		
	}

}
