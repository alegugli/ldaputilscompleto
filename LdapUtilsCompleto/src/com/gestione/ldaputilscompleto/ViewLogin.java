package com.gestione.ldaputilscompleto;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.HorizontalLayout;

public class ViewLogin extends VerticalLayout implements View
{	
	private static final long serialVersionUID = 1L;
	private Navigator navigator;
	private VerticalLayout vLayout;
	private HorizontalLayout hLayout;
	private TextField textUsername;
	private PasswordField textPassword;
	private Label labelDominio;
	private Button buttonLogin;
	private Label labelResponse;
	private Utente user;	
	private LdapLogger logger;
	
	public ViewLogin(Navigator n, LdapLogger l)
	{
		navigator = n;	
		logger = l;
	}
	
	@Override
	public void enter(ViewChangeEvent event) 
	{
		vLayout = new VerticalLayout();
		vLayout.setMargin(true);
		hLayout = new HorizontalLayout();
		textUsername = new TextField("Username");
		textUsername.setWidth("250px");
		labelDominio = new Label("&nbsp;&nbsp;<b> @ irccsme.it</b>", ContentMode.HTML);		
		textPassword = new PasswordField("Password");
		labelResponse = new Label();
		
		buttonLogin = new Button("Login");
		buttonLogin.addClickListener(new Button.ClickListener() {
			
			private static final long serialVersionUID = 1L;

			public void buttonClick(ClickEvent event) 
			{
				if(!textUsername.getValue().equals("") && !textPassword.getValue().equals(""))
				{
					user = new Utente();
					user.setUsername(textUsername.getValue());
					user.setPlainPassword(textPassword.getValue());
					
					ManageLdap manLdap = new ManageLdap(user, logger);
					if(manLdap.authAsAdmin())
					{					
						VaadinSession.getCurrent().setAttribute("admin", "1");
						logger.getOperator().setUsername(user.getUsername());
						navigator.navigateTo("ViewAdmin");
					}					
					else
					{	
						labelResponse.setValue("Credenziali errate.");					
					}
				}
				else
				{
					labelResponse.setValue("Campi mancanti.");
				}				
			}
		});
		hLayout.addComponent(textUsername);
		hLayout.addComponent(labelDominio);
		hLayout.setComponentAlignment(labelDominio, Alignment.BOTTOM_CENTER);
		vLayout.addComponent(hLayout);
		vLayout.addComponent(textPassword);
		vLayout.addComponent(new Label("<br>", ContentMode.HTML));
		vLayout.addComponent(buttonLogin);
		vLayout.addComponent(new Label("<br>", ContentMode.HTML));
		vLayout.addComponent(labelResponse);
		addComponent(vLayout);		
	}

}
