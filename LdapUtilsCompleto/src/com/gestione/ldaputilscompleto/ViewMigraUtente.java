package com.gestione.ldaputilscompleto;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Reindeer;

// View per la migrazione di utente da un OU ad un altro
public class ViewMigraUtente extends VerticalLayout implements View {
	
	private static final long serialVersionUID = 8146623592997599149L;
	private Navigator navigator;
	private VerticalLayout vLayout;
	private Button buttonHome;	// Link to home	
	private ComboBox comboUtente;
	private ComboBox comboOu;
	private Button buttonMigra;	
	private Label labelResponse;
	private String isAdmin;
	private ManageLdap manLdap;
	private Utente user;
	private boolean isLoaded;
	private LdapLogger logger;
	
	public ViewMigraUtente(Navigator n, LdapLogger l)
	{
		navigator = n;
		logger = l;
		isAdmin = "";
		isLoaded = false;
	}
	
	@Override
	public void enter(ViewChangeEvent event) {	
		
		if(isLoaded)
		{
			vLayout.removeAllComponents();
			removeComponent(vLayout);
		}
		
		vLayout = new VerticalLayout();
		vLayout.setMargin(true);
		if(VaadinSession.getCurrent().getAttribute("admin") != null)
			isAdmin = (String) VaadinSession.getCurrent().getAttribute("admin");
		if(isAdmin == "1")
		{
			user = new Utente();
			manLdap = new ManageLdap(user, logger);
			buttonHome = new Button("Admin Home");
			buttonHome.addStyleName(Reindeer.BUTTON_LINK);			
			
			comboUtente = new ComboBox("Utente (digita per cercare)");
			comboUtente.setWidth("300px");
			comboUtente.addItems(manLdap.getAllUsersList());
			
			comboOu = new ComboBox("Ou");			
			comboOu.setWidth("150px");
			comboOu.addItems(manLdap.getOuList());		
			
			buttonMigra = new Button("Migra utente");
			labelResponse = new Label();
			labelResponse.setContentMode(ContentMode.HTML);
			
			buttonHome.addClickListener(new Button.ClickListener() 
			{			    				
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) 
				{
					navigator.navigateTo("ViewAdmin");																								
				}
			});
			
			buttonMigra.addClickListener(new Button.ClickListener() 
			{			    				
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) 
				{
					if(comboUtente.getValue() != null && comboOu.getValue() != null)
					{
						user.setUsername(comboUtente.getValue().toString());
						user.setOu(comboOu.getValue().toString());
						manLdap.migrateUser();
						labelResponse.setValue("<p>Utente <b>" + user.getUsername() + "</b> migrato in ou <b>" + user.getOu() + "</b></p>");
					}
					else
					{
						labelResponse.setValue("Campi mancanti.");
					}																														
				}
			});
			
			vLayout.addComponent(buttonHome);
			vLayout.addComponent(new Label("<br>", ContentMode.HTML));			
			vLayout.addComponent(comboUtente);
			vLayout.addComponent(comboOu);
			vLayout.addComponent(new Label("<br>", ContentMode.HTML));
			vLayout.addComponent(buttonMigra);
			vLayout.addComponent(new Label("<br>", ContentMode.HTML));
			vLayout.addComponent(labelResponse);			
		}
		else
		{
			vLayout.addComponent(new Label("Accesso negato. E' necessaria l'<a href=\"/LdapUtilsCompleto/\">autenticazione</a>.", ContentMode.HTML));
		}
		addComponent(vLayout);
		isLoaded = true;
		
		
	}

}
