package com.gestione.ldaputilscompleto;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Reindeer;

public class ViewAdmin extends VerticalLayout implements View 
{	
	private static final long serialVersionUID = -4910809424314075697L;
	private Navigator navigator;
	private VerticalLayout vLayout;
	private Button linkCreaNuovoUtente;
	private Button linkCambiaGruppoUtente;
	private Button linkResetPasswordUtente;
	private Button linkEliminaUtente;
	private Button linkLeggiLogger;
	private Button linkInfoUtente;
	private Button linkMigraUtente;
	private Button linkLogout;
	private Button linkInfoApp;
	private String isAdmin;
	private boolean isLoaded;
	
	
	public ViewAdmin(Navigator n)
	{
		navigator = n;	
		isAdmin = "";
		isLoaded = false;
	}

	@Override
	public void enter(ViewChangeEvent event) 
	{	
		if(isLoaded)
		{
			vLayout.removeAllComponents();
			removeComponent(vLayout);
		}
		
		if(VaadinSession.getCurrent().getAttribute("admin") != null)
			isAdmin = (String) VaadinSession.getCurrent().getAttribute("admin");
		vLayout = new VerticalLayout();
		vLayout.setMargin(true);		
		if(isAdmin == "1")
		{				
			linkCreaNuovoUtente = new Button("Crea nuovo utente");
			linkCreaNuovoUtente.setStyleName(Reindeer.BUTTON_LINK);
			linkCreaNuovoUtente.addClickListener(new Button.ClickListener() 
			{			    				
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) 
				{
					navigator.navigateTo("ViewCreaNuovoUtente");										
				}
			});
			
			linkCambiaGruppoUtente = new Button("Cambia gruppo ad utente");
			linkCambiaGruppoUtente.setStyleName(Reindeer.BUTTON_LINK);
			linkCambiaGruppoUtente.addClickListener(new Button.ClickListener() 
			{			    				
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) 
				{
					navigator.navigateTo("ViewCambiaGruppoUtente");										
				}
			});
			
			linkResetPasswordUtente = new Button("Reset password ad utente");
			linkResetPasswordUtente.setStyleName(Reindeer.BUTTON_LINK);
			linkResetPasswordUtente.addClickListener(new Button.ClickListener() 
			{			    				
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) 
				{
					navigator.navigateTo("ViewResetPasswordUtente");										
				}
			});
			
			linkMigraUtente = new Button("Migra utente in ou diversa");
			linkMigraUtente.setStyleName(Reindeer.BUTTON_LINK);
			linkMigraUtente.addClickListener(new Button.ClickListener() 
			{			    				
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) 
				{
					navigator.navigateTo("ViewMigraUtente");										
				}
			});
			
			linkEliminaUtente = new Button("Elimina utente");
			linkEliminaUtente.setStyleName(Reindeer.BUTTON_LINK);
			linkEliminaUtente.addClickListener(new Button.ClickListener() 
			{			    				
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) 
				{
					navigator.navigateTo("ViewEliminaUtente");										
				}
			});
			
			linkInfoUtente = new Button("Info utente");
			linkInfoUtente.setStyleName(Reindeer.BUTTON_LINK);
			linkInfoUtente.addClickListener(new Button.ClickListener() 
			{			    				
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) 
				{
					navigator.navigateTo("ViewInfoUtente");										
				}
			});
			
			linkLeggiLogger = new Button("_LOG_Eventi");
			linkLeggiLogger.setStyleName(Reindeer.BUTTON_LINK);
			linkLeggiLogger.addClickListener(new Button.ClickListener() 
			{			    				
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) 
				{
					navigator.navigateTo("ViewLeggiLogger");										
				}
			});
			
			linkInfoApp = new Button("Informazioni App");
			linkInfoApp.setStyleName(Reindeer.BUTTON_LINK);
			linkInfoApp.addClickListener(new Button.ClickListener() 
			{			    				
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) 
				{
					// Create a sub-window and set the content
			        Window subWindow = new Window("Informazioni App");
			        VerticalLayout subContent = new VerticalLayout();
			        subContent.setWidth("500px");
			        subContent.setHeight("350px");
			        subContent.setMargin(true);
			        subWindow.setContent(subContent);

			        // Put some components in it
			        subContent.addComponent(new Label("<p>Creato da:</p>"
			        		+ "<p><b>Alessio Guglielmo</b> - <a href=\"mailto:alessio.guglielmo@irccsme.it\">mail</a></p>"
			        		+ "<p><b>Maurizio Paone</b> - <a href=\"mailto:maurizio.paone@irccsme.it\">mail</a></p>"
			        		+ "<hr /><p></p>"
			        		+ "<p><b>supporto tecnico</b> - <a href=\"mailto:admin@irccsme.it\">mail</a></p>", ContentMode.HTML));			        

			        // Center it in the browser window
			        subWindow.center();

			        // Open it in the UI			        
			        UI.getCurrent().addWindow(subWindow);										
				}
			});
			
			linkLogout = new Button("Logout");
			linkLogout.setStyleName(Reindeer.BUTTON_LINK);
			linkLogout.addClickListener(new Button.ClickListener() 
			{			    				
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) 
				{
					navigator.navigateTo("ViewLogout");										
				}
			});
			
			vLayout.addComponent(linkCreaNuovoUtente);			
			vLayout.addComponent(linkInfoUtente);
			vLayout.addComponent(linkCambiaGruppoUtente);			
			vLayout.addComponent(linkResetPasswordUtente);
			vLayout.addComponent(linkMigraUtente);
			vLayout.addComponent(linkEliminaUtente);
			vLayout.addComponent(linkLeggiLogger);
			vLayout.addComponent(linkInfoApp);
			vLayout.addComponent(new Label("<br>", ContentMode.HTML));
			vLayout.addComponent(linkLogout);
		}
		else
		{
			vLayout.addComponent(new Label("Accesso negato. E' necessaria l'autenticazione."));
		}
		addComponent(vLayout);
		isLoaded = true;
	}
}
