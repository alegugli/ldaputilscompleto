package com.gestione.ldaputilscompleto;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;
import com.vaadin.ui.Button.ClickEvent;

public class ViewCreaNuovoUtente extends VerticalLayout implements View 
{	
	private static final long serialVersionUID = -5005597730528072634L;
	private Navigator navigator;
	private VerticalLayout vLayout;
	private Button buttonHome;	// Link to home
	private TextField textNome;
	private TextField textCognome;
	private TextField textUsername;
	private ComboBox comboOu;
	private ComboBox comboGruppo;
	private Button buttonCrea;	
	private Label labelResponse;
	private String isAdmin;
	private ManageLdap manLdap;
	private Utente user;
	private boolean isLoaded;
	private LdapLogger logger;
	
	public ViewCreaNuovoUtente(Navigator n, LdapLogger l)
	{
		navigator = n;
		logger = l;
		isAdmin = "";
		isLoaded = false;
	}
	
	@Override
	public void enter(ViewChangeEvent event) 
	{		
		if(isLoaded)
		{
			vLayout.removeAllComponents();
			removeComponent(vLayout);
		}
		
		vLayout = new VerticalLayout();
		vLayout.setMargin(true);
		if(VaadinSession.getCurrent().getAttribute("admin") != null)
			isAdmin = (String) VaadinSession.getCurrent().getAttribute("admin");
		if(isAdmin == "1")
		{
			user = new Utente();
			manLdap = new ManageLdap(user, logger);
			buttonHome = new Button("Admin Home");
			buttonHome.addStyleName(Reindeer.BUTTON_LINK);
			textNome = new TextField("Nome");
			textCognome = new TextField("Cognome");	
			textUsername = new TextField("Username");
			comboOu = new ComboBox("ou");
			comboOu.addItems(manLdap.getOuList());			
			comboGruppo = new ComboBox("Gruppo");
			comboGruppo.addItem("nessuno");					
			buttonCrea = new Button("Crea");
			labelResponse = new Label();
				
			buttonHome.addClickListener(new Button.ClickListener() 
			{			    				
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) 
				{
					navigator.navigateTo("ViewAdmin");																								
				}
			});
								
			comboOu.addValueChangeListener(new Property.ValueChangeListener() {
				
	            private static final long serialVersionUID = -5188369735622627751L;
	           
				@Override
				public void valueChange(ValueChangeEvent event) {	
					comboGruppo.removeAllItems();
					user.setOu(comboOu.getValue().toString());
					comboGruppo.addItem("nessuno");
					comboGruppo.addItems(manLdap.getGroupsList());					
				}
	        });
			
			buttonCrea.addClickListener(new Button.ClickListener() 
			{			    				
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) 
				{
					if(textNome.getValue() != "" && textCognome.getValue() != "" && textUsername.getValue() != "" && comboGruppo.getValue() != null && comboOu.getValue() != null)							
					{						
						user.setNome(textNome.getValue());
						user.setCognome(textCognome.getValue());
						user.setUsername(textUsername.getValue());
						user.setGruppo(comboGruppo.getValue().toString());
						user.setOu(comboOu.getValue().toString());
						if(!manLdap.userExist())
						{
							manLdap.addUser();
							manLdap.addUserToGroup();
							labelResponse.setValue("<br><p>Nuovo Utente creato.</p><p>Password <b>" + user.getPlainPassword() +"</b> generata per utente <b>" + user.getUsername() + "<b/></p>");								
						}
						else
						{
							labelResponse.setValue("<br>Username gi� presente.");	
						}
						labelResponse.setContentMode(ContentMode.HTML);						
					}
					else
					{
						labelResponse.setValue("uno o pi� campi vuoti");
					}																					
				}
			});
			
			vLayout.addComponent(buttonHome);
			vLayout.addComponent(new Label("<br>", ContentMode.HTML));
			vLayout.addComponent(textNome);
			vLayout.addComponent(textCognome);
			vLayout.addComponent(textUsername);
			vLayout.addComponent(comboOu);		
			vLayout.addComponent(comboGruppo);
			vLayout.addComponent(new Label("<br>", ContentMode.HTML));
			vLayout.addComponent(buttonCrea);
			vLayout.addComponent(labelResponse);
		}
		else
		{
			vLayout.addComponent(new Label("Accesso negato. E' necessaria l'<a href=\"/LdapUtilsCompleto/\">autenticazione</a>.", ContentMode.HTML));
		}
		addComponent(vLayout);
		isLoaded = true;
	}

}
