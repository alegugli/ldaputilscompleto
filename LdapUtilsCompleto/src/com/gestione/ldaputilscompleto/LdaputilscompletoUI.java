package com.gestione.ldaputilscompleto;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.*;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@Theme("ldaputilscompleto")
public class LdaputilscompletoUI extends UI {
	
	private Navigator navigator;	
	private static LdapLogger logger = null;
	protected static final String viewAdmin = "ViewAdmin";
	protected static final String viewCreaNuovoUtente = "ViewCreaNuovoUtente";
	protected static final String viewCambiaGruppoUtente = "ViewCambiaGruppoUtente";
	protected static final String viewResetPasswordUtente = "ViewResetPasswordUtente";
	protected static final String viewEliminaUtente = "ViewEliminaUtente";
	protected static final String viewLeggiLogger = "ViewLeggiLogger";
	protected static final String viewCambioPassword = "ViewCambioPassword";
	protected static final String viewInfoUtente = "ViewInfoUtente";
	protected static final String viewMigraUtente = "ViewMigraUtente";
	protected static final String viewLogout = "ViewLogout";
	private VerticalLayout vLayout;
		
	@WebServlet(urlPatterns = {"/*", "/VAADIN/*"}, asyncSupported = true)		
	@VaadinServletConfiguration(productionMode = false, ui = LdaputilscompletoUI.class)
	public static class Servlet extends VaadinServlet 
	{
		@Override
	    protected void servletInitialized() throws javax.servlet.ServletException {
	        super.servletInitialized();
	        
	        VaadinService.getCurrent().addSessionDestroyListener(new SessionDestroyListener() {

				@Override
				public void sessionDestroy(SessionDestroyEvent event) {
					logger.info("Logout");										
					logger.getMongoClient().close();					
				}	            
	        });
	    }
	}
			
	@Override
	protected void init(VaadinRequest request) 
	{
		vLayout = new VerticalLayout();
		vLayout.setMargin(true);
		setContent(vLayout);
		
		getPage().setTitle("LDAP Utils");
        				
		logger = new LdapLogger();							
		
        navigator = new Navigator(this, this);
               
        navigator.addView("", new ViewLogin(navigator, logger));
        navigator.addView(viewAdmin, new ViewAdmin(navigator));
        navigator.addView(viewCreaNuovoUtente, new ViewCreaNuovoUtente(navigator, logger));
        navigator.addView(viewCambiaGruppoUtente, new ViewCambiaGruppoUtente(navigator, logger));
        navigator.addView(viewResetPasswordUtente, new ViewResetPasswordUtente(navigator, logger));
        navigator.addView(viewEliminaUtente, new ViewEliminaUtente(navigator, logger));        
        navigator.addView(viewLeggiLogger, new ViewLeggiLogger(navigator, logger));
        navigator.addView(viewCambioPassword, new ViewCambioPassword(logger));
        navigator.addView(viewInfoUtente, new ViewInfoUtente(navigator, logger));                
        navigator.addView(viewMigraUtente, new ViewMigraUtente(navigator, logger));
        navigator.addView(viewLogout, new ViewLogout());
	}
}