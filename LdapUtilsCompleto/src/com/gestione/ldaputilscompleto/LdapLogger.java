package com.gestione.ldaputilscompleto;

import java.net.UnknownHostException;
import java.util.Date;
import org.apache.log4j.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.vaadin.ui.Label;

public class LdapLogger 
{		
	private final static String serverMongo = "10.0.30.19";
	private final static String database = "dblogs";
	private MongoClient mongoClient;
	private DB db;
	private final static Logger log = Logger.getLogger(LdapLogger.class.getName());	
	private Utente operator;
		 
	public LdapLogger()
	{		 	
		mongoClient = connectMongoDb();
		setOperator(new Utente());		
	}	
	
	private MongoClient connectMongoDb()
	{
		// Inizializzazione Log su mongoDb per lo storico degli eventi scalabile.
		MongoClient mc = null;
		try
	    {	    	
	    	mc = new MongoClient(new ServerAddress(serverMongo));		    
		    db = mc.getDB(database);
		    return mc;
	    }
	    catch(UnknownHostException ex)
	    {
	    	ex.printStackTrace();
	    }
		return mc;
	}
	
	public MongoClient getMongoClient()
	{
		return mongoClient;
	}
	
	public Logger getLog()
	{
		return log;
	}
	
	public void writeLogMongo(String level, String msg)
	{
		if(db == null)
			System.out.print("null db");
		if(mongoClient == null)
			System.out.println("null mongo");
		DBCollection coll = db.getCollection("logs");
		Date date = new Date();		
		BasicDBObject doc = new BasicDBObject("date", date.toString())
				.append("level", level)
				.append("message", msg)
				.append("timestamp", System.currentTimeMillis());		        		        	      
		coll.insert(doc);
	}
	
	public void info(String msg)
	{
		log.info(msg);
		writeLogMongo("info", msg);		
	}
	
	public void warn(String msg)
	{
		log.warn(msg);
		writeLogMongo("warning", msg);
	}
	
	public void error(String msg)
	{
		log.warn(msg);
		writeLogMongo("error", msg);
	}
		
	public void printMongoLog(Label labelOutput)
	{
		// Dovrebbe essere catturato in ordine DESC
		DBCollection coll = db.getCollection("logs");
		DBCursor cursor = coll.find().sort(new BasicDBObject("timestamp", -1));		
		try 
		{
		   while(cursor.hasNext()) 
		   {
		       DBObject o = cursor.next();
		       labelOutput.setValue(labelOutput.getValue() + "<p></p>" + o.get("date") + "<br>" + o.get("level") + " - " + o.get("message"));		       
		   }
		} 
		finally {
		   cursor.close();
		}
	}

	public Utente getOperator() {
		return operator;
	}

	public void setOperator(Utente operator) {
		this.operator = operator;
	}				
}
