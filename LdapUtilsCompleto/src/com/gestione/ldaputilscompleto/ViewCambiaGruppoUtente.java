package com.gestione.ldaputilscompleto;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Reindeer;

public class ViewCambiaGruppoUtente extends VerticalLayout implements View
{	
	private static final long serialVersionUID = 250271568448983068L;
	private Navigator navigator;
	private VerticalLayout vLayout;
	private HorizontalLayout hLayout;
	private Button buttonHome;
	private ComboBox comboOu;
	private ComboBox comboUtente;
	private Label labelGruppo;
	private ComboBox comboGruppo;
	private Button buttonCambia;
	private Label labelResponse;
	private String isAdmin;
	private ManageLdap manLdap;
	private Utente user;
	private boolean isLoaded;
	private LdapLogger logger;
	
	public ViewCambiaGruppoUtente(Navigator n, LdapLogger l)
	{
		navigator = n;	
		logger = l;
		isAdmin = "";		
		isLoaded = false;
	}
			
	@Override
	public void enter(ViewChangeEvent event) 
	{		
		if(isLoaded)
		{
			vLayout.removeAllComponents();
			removeComponent(vLayout);			
		}
		
		vLayout = new VerticalLayout();
		vLayout.setMargin(true);
		if(VaadinSession.getCurrent().getAttribute("admin") != null)
			isAdmin = (String) VaadinSession.getCurrent().getAttribute("admin");		
			
		if(isAdmin == "1")
		{
			user = new Utente();
			manLdap = new ManageLdap(user, logger);
			buttonHome = new Button("Admin Home");
			buttonHome.addStyleName(Reindeer.BUTTON_LINK);
			comboOu = new ComboBox("ou");
			comboOu.addItems(manLdap.getOuList());			
			comboUtente = new ComboBox("Utente (digita per cercare)");
			comboUtente.setWidth("300px");	
			labelGruppo = new Label();
			labelGruppo.setContentMode(ContentMode.HTML);
			hLayout = new HorizontalLayout();
			hLayout.addComponent(comboUtente);
			hLayout.addComponent(labelGruppo);
			comboGruppo = new ComboBox("Sposta in gruppo");	
			comboGruppo.setWidth("300px");
			comboGruppo.addItem("nessuno");
			comboGruppo.addItems(manLdap.getGroupsList());			
			buttonCambia = new Button("Cambia gruppo");
			labelResponse = new Label();
			labelResponse.setContentMode(ContentMode.HTML);
			
			buttonHome.addClickListener(new Button.ClickListener() 
			{			    				
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) 
				{
					navigator.navigateTo("ViewAdmin");																								
				}
			});
			
			comboOu.addValueChangeListener(new Property.ValueChangeListener() {

				private static final long serialVersionUID = -5188369735622627751L;

				@Override
				public void valueChange(ValueChangeEvent event) {
					labelGruppo.setValue("");
					if(comboOu.getValue() != null)
					{
						comboUtente.removeAllItems();
						comboGruppo.removeAllItems();
						user.setOu(comboOu.getValue().toString());
						comboUtente.addItems(manLdap.getGenericUsersList());
						comboGruppo.addItem("nessuno");
						comboGruppo.addItems(manLdap.getGroupsList());						
					}									
				}
			});
			
			comboUtente.addValueChangeListener(new Property.ValueChangeListener() {

				private static final long serialVersionUID = -7852090743298194251L;

				@Override
				public void valueChange(ValueChangeEvent event) {
					if(comboUtente.getValue() != null)
					{
						user.setUsername(comboUtente.getValue().toString());
						manLdap.getUserGroup();
						labelGruppo.setValue("&nbsp;&nbsp; � nel gruppo <b>" + user.getGruppo() + "</b>");
					}								
				}
			});
			
			buttonCambia.addClickListener(new Button.ClickListener() 
			{			    				
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) 
				{
					if(comboUtente.getValue() != null && comboGruppo.getValue() != null && comboOu.getValue() != null)
					{
						if(!comboUtente.getValue().equals("") && !comboGruppo.getValue().equals(""))
						{
							user.setUsername(comboUtente.getValue().toString());
							user.setGruppo(comboGruppo.getValue().toString());
							manLdap.removeUserFromAllGroups();
							user.setGruppo(comboGruppo.getValue().toString());
							manLdap.addUserToGroup();
							labelResponse.setValue("<br><p>Utente <b>" + user.getUsername() + "</b> spostato in <b>" + user.getGruppo() + "</b></p>");
						}						
					}					
					else
					{
						labelResponse.setValue("<br><p>Campi vuoti, ricontrollare.</p>");
					}					
				}
			});
			vLayout.addComponent(buttonHome);
			vLayout.addComponent(new Label("<br>", ContentMode.HTML));
			vLayout.addComponent(comboOu);
			vLayout.addComponent(new Label("<br>", ContentMode.HTML));
			vLayout.addComponent(hLayout);
			hLayout.setComponentAlignment(labelGruppo, Alignment.BOTTOM_CENTER);
			vLayout.addComponent(new Label("<br>", ContentMode.HTML));
			vLayout.addComponent(comboGruppo);
			vLayout.addComponent(new Label("<br>", ContentMode.HTML));
			vLayout.addComponent(buttonCambia);
			vLayout.addComponent(labelResponse);
		}
		else
		{
			vLayout.addComponent(new Label("Accesso negato. E' necessaria l'<a href=\"/LdapUtilsCompleto/\">autenticazione</a>.", ContentMode.HTML));
		}		
		addComponent(vLayout);
		isLoaded = true;
	}

}
