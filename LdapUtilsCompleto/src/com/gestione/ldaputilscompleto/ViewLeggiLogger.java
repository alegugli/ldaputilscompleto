package com.gestione.ldaputilscompleto;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Reindeer;

public class ViewLeggiLogger extends VerticalLayout implements View {
		
	private static final long serialVersionUID = -8844845861938194102L;
	private Navigator navigator;
	private VerticalLayout vLayout;
	private Button buttonHome;
	private Label labelOutput;	
	private String isAdmin;
	private boolean isLoaded;
	private LdapLogger logger;
	
	public ViewLeggiLogger(Navigator n, LdapLogger l)
	{
		this.navigator = n;
		logger = l;
		isAdmin = "";
		isLoaded = false;
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		if(isLoaded)
		{
			vLayout.removeAllComponents();
			removeComponent(vLayout);
		}
		
		vLayout = new VerticalLayout();
		vLayout.setMargin(true);
		if(VaadinSession.getCurrent().getAttribute("admin") != null)
			isAdmin = (String) VaadinSession.getCurrent().getAttribute("admin");		
		
		if(isAdmin == "1")
		{
			buttonHome = new Button("Admin Home");
			buttonHome.addStyleName(Reindeer.BUTTON_LINK);
			labelOutput = new Label();
			labelOutput.setContentMode(ContentMode.HTML);
						
			logger.printMongoLog(labelOutput);
			
			buttonHome.addClickListener(new Button.ClickListener() 
			{			    				
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) 
				{
					navigator.navigateTo("ViewAdmin");																								
				}
			});	
			vLayout.addComponent(buttonHome);
			vLayout.addComponent(new Label("<br>", ContentMode.HTML));			
			vLayout.addComponent(labelOutput);	
		}
		else
		{
			vLayout.addComponent(new Label("Accesso negato. E' necessaria l'<a href=\"/LdapUtilsCompleto/\">autenticazione</a>.", ContentMode.HTML));
		}
		addComponent(vLayout);
		isLoaded = true;		
	}
}
