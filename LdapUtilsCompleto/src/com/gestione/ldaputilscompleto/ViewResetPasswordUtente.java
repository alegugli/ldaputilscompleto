package com.gestione.ldaputilscompleto;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Reindeer;

public class ViewResetPasswordUtente extends VerticalLayout implements View
{	
	private static final long serialVersionUID = 2806848180441597271L;
	private Navigator navigator;
	private VerticalLayout vLayout;
	private Button buttonHome;
	private ComboBox comboUtente;
	private Button buttonReset;
	private Label labelResponse;
	private String isAdmin;
	private ManageLdap manLdap;
	private Utente user;
	private boolean isLoaded;
	private LdapLogger logger;
	
	public ViewResetPasswordUtente(Navigator n, LdapLogger l)
	{
		navigator = n;
		logger = l;
		isAdmin = "";
		isLoaded = false;
	}

	@Override
	public void enter(ViewChangeEvent event) 
	{		
		if(isLoaded)
		{
			vLayout.removeAllComponents();
			removeComponent(vLayout);
		}
		
		vLayout = new VerticalLayout();
		vLayout.setMargin(true);
		if(VaadinSession.getCurrent().getAttribute("admin") != null)
			isAdmin = (String) VaadinSession.getCurrent().getAttribute("admin");
		if(isAdmin == "1")
		{
			user = new Utente();
			manLdap = new ManageLdap(user, logger);
			labelResponse = new Label();
			buttonHome = new Button("Admin Home");
			buttonHome.addStyleName(Reindeer.BUTTON_LINK);
			buttonHome.addClickListener(new Button.ClickListener() 
			{			    				
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) 
				{
					navigator.navigateTo("ViewAdmin");																								
				}
			});
			comboUtente = new ComboBox("Utente (digita per cercare)");			
			comboUtente.setWidth("300px");
			comboUtente.addItems(manLdap.getAllUsersList());			
			buttonReset = new Button("Reset password");
			buttonReset.addClickListener(new Button.ClickListener() 
			{			    				
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) 
				{
					if(comboUtente.getValue() != null)
					{
						user.setUsername(comboUtente.getValue().toString());
						manLdap.resetUserPassword();
						labelResponse.setValue("<br><p>Nuova password <b>" + user.getPlainPassword() +"</b> generata per utente " + user.getUsername() + "</p>");						
					}	
					else
					{
						labelResponse.setValue("Campo mancante.");
					}
				}
			});
			labelResponse.setContentMode(ContentMode.HTML);
			vLayout.addComponent(buttonHome);
			vLayout.addComponent(new Label("<br>", ContentMode.HTML));			
			vLayout.addComponent(comboUtente);
			vLayout.addComponent(new Label("<br>", ContentMode.HTML));
			vLayout.addComponent(buttonReset);
			vLayout.addComponent(labelResponse);
		}
		else
		{
			vLayout.addComponent(new Label("Accesso negato. E' necessaria l'<a href=\"/LdapUtilsCompleto/\">autenticazione</a>.", ContentMode.HTML));
		}	
		addComponent(vLayout);
		isLoaded = true;
	}

}
