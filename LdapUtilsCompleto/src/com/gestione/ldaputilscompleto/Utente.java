package com.gestione.ldaputilscompleto;

public class Utente {
	
	private String username;
	private String nome;
	private String cognome;
	private String plainPassword;
	private String sha1Password; // Password in SHA1
	private String ntPassword;	// Samba password per windows clients	
	private String gruppo;
	private final static String dc = "dc=irccsme, dc=it";
	private String dn;
	private String ou;
	private String displayName;
	
	public Utente()
	{ }
	
	public Utente(String username, String nome, String cognome, String plainPassword, String sha1Password, String ntPassword)
	{
		this.username = username;
		this.nome = nome;
		this.cognome = cognome;
		this.plainPassword = plainPassword;
		this.sha1Password = sha1Password;
		this.setNtPassword(ntPassword);
	}		
	
	public String getUsername()
	{
		return username;
	}
	
	public void setUsername(String username)
	{
		String[] mail = username.split("@");
		this.username = mail[0];
	}
	
	public String getPlainPassword()
	{
		return plainPassword;
	}
	
	public void setPlainPassword(String plainPassword)
	{
		this.plainPassword = plainPassword;
	}
	
	public String getSha1Password()
	{
		return sha1Password;
	}
	
	public void setSha1Password(String sha1Password)
	{
		this.sha1Password = sha1Password;
	}
	
	public String getNome()
	{
		return this.nome;
	}
	
	public void setNome(String nome)
	{
		this.nome = nome;
	}
	
	public void setCognome(String cognome)
	{
		this.cognome = cognome;
	}
	
	public String getCognome()
	{
		return this.cognome;
	}
	
	public String getUserDN()
	{
		return "uid=" + username + ", ou=" + ou + "," + dc;
	}
	
	public String getExternalUserDN()
	{
		return "uid=" + username + ", ou=external, dc=irccsme, dc=it";
	}

	public String getNtPassword() {
		return ntPassword;
	}

	public void setNtPassword(String ntPassword) {
		this.ntPassword = ntPassword;
	}

	public String getGruppo() {
		return gruppo;
	}

	public void setGruppo(String gruppo) {
		this.gruppo = gruppo;
	}

	public String getDn() {
		return dn;
	}

	public void setDn(String dn) {
		this.dn = dn;
	}

	public String getOu() {
		return ou;
	}

	public void setOu(String ou) {
		this.ou = ou;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}			
}
