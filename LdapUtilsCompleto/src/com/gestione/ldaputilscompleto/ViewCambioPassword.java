package com.gestione.ldaputilscompleto;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Reindeer;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;

public class ViewCambioPassword extends VerticalLayout implements View {
		
	private static final long serialVersionUID = -5919617099686171583L;	
	private VerticalLayout vLayout;	
	private HorizontalLayout hLayout;
	private HorizontalLayout headerHLayout;
	private Image imageLogo;
	private Button linkInfoApp;
	private Label labelDominio;
	private TextField textUsername;
	private PasswordField textOldPassword;
	private PasswordField textNewPassword1;
	private PasswordField textNewPassword2;
	private Button buttonCambia;
	private Label labelResponse;	
	private boolean isLoaded;
	private ManageLdap manLdap;
	private Utente user;
	private LdapLogger logger;

	public ViewCambioPassword(LdapLogger l)
	{				
		user = new Utente();
		logger = l;		
		manLdap = new ManageLdap(user, logger);
		isLoaded = false;
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		if(isLoaded)
		{
			vLayout.removeAllComponents();
			removeComponent(vLayout);
		}
		
		vLayout = new VerticalLayout();
		vLayout.setMargin(true);
		hLayout = new HorizontalLayout();
		
		Panel panel = new Panel();
		panel.setStyleName("panelCambioPassword");
		panel.setWidth("360px");
						
		// Image as a file resource
		ThemeResource resource = new ThemeResource("img/logo.jpg");		

		// Show the image in the application
		imageLogo = new Image("", resource);
		imageLogo.setWidth("150px");
		imageLogo.setHeight("60px");
		
		linkInfoApp = new Button("Info App");
		linkInfoApp.setStyleName(Reindeer.BUTTON_LINK);
		linkInfoApp.addClickListener(new Button.ClickListener() 
		{			    				
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) 
			{
				// Create a sub-window and set the content
		        Window subWindow = new Window("Informazioni App");
		        VerticalLayout subContent = new VerticalLayout();
		        subContent.setWidth("350px");
		        subContent.setHeight("200px");
		        subContent.setMargin(true);
		        subWindow.setContent(subContent);

		        // Put some components in it
		        subContent.addComponent(new Label("<p>Creato da:</p>"
		        		+ "<p><b>Alessio Guglielmo</b> - <a href=\"mailto:alessio.guglielmo@irccsme.it\">mail</a></p>"
		        		+ "<p><b>Maurizio Paone</b> - <a href=\"mailto:maurizio.paone@irccsme.it\">mail</a></p>"
		        		+ "<hr /><p></p>"
		        		+ "<p><b>supporto tecnico</b> - <a href=\"mailto:admin@irccsme.it\">mail</a></p>", ContentMode.HTML));			        

		        // Center it in the browser window
		        // subWindow.center();

		        // Open it in the UI			        
		        UI.getCurrent().addWindow(subWindow);										
			}
		});
		
		headerHLayout = new HorizontalLayout();		
		
		labelResponse = new Label();		
		labelDominio = new Label("&nbsp;&nbsp;<b> @ irccsme.it</b>", ContentMode.HTML);
		textUsername = new TextField("Username");
		textUsername.setWidth("200px");
		textUsername.setHeight("30px");
		textOldPassword = new PasswordField("Vecchia password");
		textOldPassword.setHeight("30px");
		textNewPassword1 = new PasswordField("Nuova password");
		textNewPassword1.setHeight("30px");
		textNewPassword2 = new PasswordField("Ripeti nuova password");
		textNewPassword2.setHeight("30px");
		
		panel.setContent(vLayout);
		
		buttonCambia = new Button("Cambia password");
		buttonCambia.setHeight("30px");
		buttonCambia.addClickListener(new Button.ClickListener() 
		{			    				
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) 
			{
				if(!textUsername.getValue().equals("") && !textOldPassword.getValue().equals("") && 
						!textNewPassword1.getValue().equals("") && !textNewPassword2.getValue().equals(""))
				{					
					if(textNewPassword1.getValue().equals(textNewPassword2.getValue()))
					{											
						if(textNewPassword1.getValue().length() > 4)
						{
							user.setUsername(textUsername.getValue());
							user.setPlainPassword(textOldPassword.getValue());
							if(manLdap.authAsSimpleUser())
							{
								user.setPlainPassword(textNewPassword1.getValue());
								manLdap.changeUserPassword();
								labelResponse.setValue("<p>Password cambiata correttamente.</p>");																
							}
							else
								labelResponse.setValue("<p>Credenziali errate. Impossibile cambiare la password.</p>");
						}
						else
							labelResponse.setValue("<p>Password troppo debole. La password deve essere di almeno 5 caratteri e contenere lettere e numeri.</p>");							
					}
					else
						labelResponse.setValue("<p>Le due password non coincidono.</p>");
				}
				else
					labelResponse.setValue("<p>Uno o pi� campi mancanti.</p>");
			}
		});
		labelResponse.setContentMode(ContentMode.HTML);
				
		hLayout.addComponent(textUsername);
		hLayout.addComponent(labelDominio);
		hLayout.setComponentAlignment(labelDominio, Alignment.BOTTOM_CENTER);
		headerHLayout.addComponent(imageLogo);
		headerHLayout.addComponent(linkInfoApp);
		vLayout.addComponent(headerHLayout);
		vLayout.addComponent(new Label("<br>", ContentMode.HTML));
		vLayout.addComponent(hLayout);
		vLayout.addComponent(textOldPassword);
		vLayout.addComponent(textNewPassword1);
		vLayout.addComponent(textNewPassword2);
		vLayout.addComponent(new Label("<br>", ContentMode.HTML));
		vLayout.addComponent(buttonCambia);
		vLayout.addComponent(labelResponse);
		
		addComponent(panel);
		isLoaded = true;
	}

}
