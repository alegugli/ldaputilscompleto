package com.gestione.ldaputilscompleto;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.directory.api.ldap.model.cursor.CursorException;
import org.apache.directory.api.ldap.model.cursor.EntryCursor;
import org.apache.directory.api.ldap.model.entry.*;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.message.SearchScope;
import org.apache.directory.api.ldap.model.name.Dn;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapNetworkConnection;

import jcifs.util.*;

import java.io.UnsupportedEncodingException;

import org.apache.commons.lang3.RandomStringUtils;

public class ManageLdap {
	
	private final String host = "10.0.30.13";
	private final int port = 389;
	private final static String dc = "dc=irccsme,dc=it";
	private Utente user;	
	private LdapConnection connection;
	private LdapLogger logger;
	
	public ManageLdap(Utente u, LdapLogger l)
	{ 
		this.user = u;		
		logger = l;
		try
		{
			// Autenticazione admin
			connection = new LdapNetworkConnection( host, port );
			connection.bind( "cn=admin, dc=irccsme, dc=it", "ldapone" );					
		}
		catch(LdapException ex)
		{			
			logger.warn("Problema connessione LDAP server (cn=admin)");
			ex.printStackTrace();
		}		
	}
	
	public boolean authAsAdmin()
	{				
		try
		{
			// Autenticazione utente
			if(!authAsSimpleUser())
				return false;									
			try
			{	
				user.setOu("users");
				// Verifica se utente � anche Admin					
				EntryCursor cursor = connection.search( "cn=amministratori,ou=users," + dc, "(member=" + user.getUserDN() + ")", SearchScope.SUBTREE, "*" );		
				while ( cursor.next() )
				{							
					logger.info("Autenticazione di " + user.getUsername() + " (admin) ok");						
					return true;					
				}					
			}
			catch(CursorException ex)
			{
				System.out.println(ex.getMessage());
			}	
		}
		catch(LdapException ex)
		{
			System.out.println(ex.getMessage());
		}				
		logger.warn("Autenticazione di " + user.getUsername() + " come admin non "
				+ "consentita. (Privilegi insufficienti)");		
		return false;
	}
	
	public boolean authAsSimpleUser()
	{		
		// Autenticazione utente semplice
		try
		{
			// Cerco DN utente
			try
			{												
				EntryCursor cursor = connection.search( "dc=irccsme,dc=it", "(&(objectClass=posixAccount)(uid=" + user.getUsername() +"))", SearchScope.SUBTREE, "cn" );		
				while ( cursor.next() )
				{							
					Entry e = cursor.get();
					user.setDn(e.get("cn").get().toString());					
				}			
			}
			catch(CursorException ex)
			{
				logger.error("Problema ricerca dn " + user.getUsername());
				ex.printStackTrace();
			}
			catch(LdapException ex)
			{
				logger.error("Problema ricerca dn " + user.getUsername());
				ex.printStackTrace();
			}
			
			try
			{
				LdapConnection connectionUser = new LdapNetworkConnection( host, port );
				connectionUser.bind( user.getDn(), user.getPlainPassword() );
				logger.info("Autenticazione utente " + user.getUsername() + " ok.");								
				connectionUser.unBind();
				connectionUser.close();	
				return true;
			}
			catch(LdapException ex)
			{
				System.out.println(ex.getMessage());
			}
		}
		catch(IOException ex)
		{
			System.out.println(ex.getMessage());
		}
		logger.error("Autenticazione utente " + user.getUsername() + " fallita. (credenziali errate)");		
		return false;
	}
	
	public List<String> getOuList()
	{
		List<String> ouList = new ArrayList<String>();
		// Cerco DN utente
		try
		{												
			EntryCursor cursor = connection.search( dc, "(&(objectClass=organizationalUnit))", SearchScope.SUBTREE, "ou" );		
			while ( cursor.next() )
			{							
				Entry e = cursor.get();
				ouList.add(e.get("ou").get().toString());								
			}
			return ouList;
		}
		catch(CursorException ex)
		{
			logger.error("Problema ricerca ou");
		}
		catch(LdapException ex)
		{
			logger.error("Problema ricerca ou");
		}
		logger.error("Ricerca lista OU fallita.");		
		return ouList;
	}
	
	public void addUser()
	{					
		try
		{
			this.generateRandomPassword();
			this.generateSha1();
			this.generateNTPassword();			
		}
		catch(NoSuchAlgorithmException ex)
		{
			System.out.println(ex.getMessage());
		}
		catch(UnsupportedEncodingException ex)
		{
			System.out.println(ex.getMessage());
		}
				        							
		try 
		{						
			connection.add( new DefaultEntry( 
				user.getUserDN(),
				"ObjectClass: account",
				"ObjectClass: radiusprofile",
				"ObjectClass: shadowAccount",
				"ObjectClass: sambaSamAccount",
				"ObjectClass: posixAccount",
				"ObjectClass: top",		
				"cn", user.getUserDN(),				           
				"gidNumber: 1",
				"homeDirectory: /home/" + user.getUsername(),
				"sambaSID: 1",
				"uid", user.getUsername(),
				"uidNumber: 1",
				"description", user.getNome() + " " + user.getCognome(),				            				            
				"displayName", user.getNome() + " " + user.getCognome(),
				"loginShell: /bin/sh",
				"sambaNTPassword", user.getNtPassword(),
				"userPassword", user.getSha1Password()				          				          
				));	
			logger.info("Creato nuovo utente " + user.getUsername() + " - by " + logger.getOperator().getUsername());			
		} 
		catch (LdapException e) {			
			e.printStackTrace();
		}				            													
	}
	
	// Preleva il gruppo cui appartiene un utente
	public void getUserGroup()
	{
		try
		{												
			EntryCursor cursor = connection.search( dc, "(&(objectClass=posixAccount)(uid=" + user.getUsername() +"))", SearchScope.SUBTREE, "cn" );		
			while ( cursor.next() )
			{							
				Entry e = cursor.get();
				user.setDn(e.get("cn").get().toString());					
			}			
		}
		catch(CursorException ex)
		{
			logger.error("Problema ricerca dn " + user.getUsername());			
		}
		catch(LdapException ex)
		{
			logger.error("Problema ricerca dn " + user.getUsername());			
		}
		
		try
		{			
			user.setGruppo("nessuno");						
			EntryCursor cursor = connection.search( "dc=irccsme,dc=it", "(&(objectClass=groupOfNames)(member=" + user.getDn() +"))", SearchScope.SUBTREE, "cn" );				
			while ( cursor.next() )
			{					
				Entry e = cursor.get();				
				user.setGruppo(e.get("cn").get().toString());				
			}																
		}
		catch(LdapException ex)
		{
			System.out.println(ex.getMessage());
		}
		catch(CursorException ex)
		{
			System.out.println(ex.getMessage());
		}
	}
	
	public List<String> getGroupsList()
	{
		List<String> groupList = new ArrayList<String>();
		try
		{							
			EntryCursor cursor = connection.search( "ou=" + user.getOu() + "," + dc, "(&(objectClass=groupOfNames))", SearchScope.SUBTREE, "cn" );		
			while ( cursor.next() )
			{											
				Entry e = cursor.get();					
				groupList.add(e.get("cn").get().toString());
			}			
		}
		catch(CursorException ex)
		{
			logger.error("Problema nel prelevare gruppi per OU " + user.getOu());
		}
		catch(LdapException ex)
		{
			logger.error("Problema nel prelevare gruppi per OU " + user.getOu());
		}
		return groupList;
	}
	
	// Preleva la lista di TUTTI gli utenti (ou interni ed esterni)
	public List<String> getAllUsersList()
	{
		List<String> userList = new ArrayList<String>();
		try
		{								
			EntryCursor cursor = connection.search( "dc=irccsme, dc=it", "(&(objectClass=posixAccount))", SearchScope.SUBTREE, "uid" );		
			while ( cursor.next() )
			{											
				Entry e = cursor.get();				
				userList.add(e.get("uid").get().toString());
			}			
		}
		catch(CursorException ex)
		{
			System.out.println(ex.getMessage());
		}
		catch(LdapException ex)
		{
			System.out.println(ex.getMessage());
		}
		Collections.sort(userList);
		return userList;
	}
	
	// Preleva la lista di tutti gli utenti di un dato OU
	public List<String> getGenericUsersList()
	{
		List<String> userList = new ArrayList<String>();
		try
		{								
			EntryCursor cursor = connection.search( "ou=" + user.getOu() + "," + dc, "(&(objectClass=posixAccount))", SearchScope.SUBTREE, "uid" );		
			while ( cursor.next() )
			{											
				Entry e = cursor.get();				
				userList.add(e.get("uid").get().toString());
			}			
		}
		catch(CursorException ex)
		{
			logger.error("Problema listing utenti interni");
		}
		catch(LdapException ex)
		{
			logger.error("Problema listing utenti interni");
		}
		Collections.sort(userList);
		return userList;
	}
	
	public boolean userExist()
	{		
		// Cerco DN utente
		try
		{												
			EntryCursor cursor = connection.search( "dc=irccsme,dc=it", "(&(objectClass=posixAccount)(uid=" + user.getUsername() +"))", SearchScope.SUBTREE, "*" );		
			while ( cursor.next() )
			{							
				return true;					
			}			
		}
		catch(CursorException ex)
		{
			logger.error("Problema ricerca utente " + user.getUsername());
		}
		catch(LdapException ex)
		{
			logger.error("Problema ricerca utente " + user.getUsername());
		}
		return false;
	}
	
	public void removeUser()
	{
		try 
		{
			connection.delete( user.getDn() );
			logger.info("Eliminato utente " + user.getUsername() + " " + user.getDn() + " - by " + logger.getOperator().getUsername());			
		} 
		catch (LdapException e) {			
			logger.error("Problema eliminazione utente " + user.getUsername());
		}			
	}
	
	public void migrateUser()
	{
		// Cerco DN utente
		try
		{			
			Entry userEntry = null;
			EntryCursor cursor = connection.search( dc, "(&(objectClass=posixAccount)(uid=" + user.getUsername() +"))", SearchScope.SUBTREE, "*" );		
			while ( cursor.next() )
			{							
				userEntry = cursor.get();
				user.setDn(userEntry.get("cn").get().toString());					
			}
			// Clona entry utente cambiando dn e cn
			Entry cloneUser = userEntry.clone();					
			cloneUser.setDn("uid=" + user.getUsername() + ",ou=" + user.getOu() + "," + dc);
			// Pulisce il vecchio valore cn
			cloneUser.get("cn").clear();
			// Aggiunge il nuovo cn
			cloneUser.get("cn").add(("uid=" + user.getUsername() + ",ou=" + user.getOu() + "," + dc));
			// Rimuove l'utente da tutti i gruppi cui � affiliato nel vecchio OU
			removeUserFromAllGroups();
			// Aggiunge utente al nuovo ou
			connection.add(cloneUser);						
			//Rimuove utente da vecchio OU
			removeUser();
			
			logger.info("Migrato utente " + user.getUsername() + " su " + user.getOu() + " - by " + logger.getOperator().getUsername());
		}
		catch(CursorException ex)
		{
			logger.error("Problema migrazione ou per dn " + user.getUsername());			
		}
		catch(LdapException ex)
		{
			logger.error("Problema migrazione ou per dn " + user.getUsername());			
		}
		catch(Exception ex)
		{
			logger.error("Problema migrazione ou per dn " + user.getUsername());
		}
	}
	
	public void getUserCredentials()
	{
		try
		{												
			EntryCursor cursor = connection.search( "dc=irccsme,dc=it", "(&(objectClass=posixAccount)(uid=" + user.getUsername() +"))", SearchScope.SUBTREE, "*" );		
			while ( cursor.next() )
			{							
				Entry e = cursor.get();
				user.setDisplayName(e.get("displayName").get().toString());
				user.setNtPassword(e.get("sambaNTPassword").get().toString());																					
				user.setSha1Password(e.get("userPassword").get().toString());
			}			
		}
		catch(CursorException ex)
		{
			logger.error("Problema get credenziali per " + user.getUsername());
		}
		catch(LdapException ex)
		{
			logger.error("Problema get credenziali per " + user.getUsername());
		}				
		catch(Exception ex)
		{
			logger.error("Problema get credenziali per " + user.getUsername());			
		}
	}
	
	public void addUserToGroup()
	{			
		if(!user.getGruppo().equals("nessuno"))
		{
			try
			{
				Modification addedMember = new DefaultModification( ModificationOperation.ADD_ATTRIBUTE, "member", user.getUserDN() );
				connection.modify( "cn=" + user.getGruppo() + ",ou=" + user.getOu() + "," + dc, addedMember );
				logger.info("Utente " + user.getUsername() + " aggiunto a gruppo " + user.getGruppo() + " - by " + logger.getOperator().getUsername());				
			}
			catch(LdapException ex)
			{
				logger.error("Problema di adding al gruppo " + user.getGruppo() + " dell'utente " + user.getUsername());
			}
		}				
	}
	
	public void removeUserFromAllGroups()
	{
		// Cerco DN utente
		try
		{												
			EntryCursor cursor = connection.search( dc, "(&(objectClass=posixAccount)(uid=" + user.getUsername() +"))", SearchScope.SUBTREE, "cn" );		
			while ( cursor.next() )
			{							
				Entry e = cursor.get();
				user.setDn(e.get("cn").get().toString());					
			}			
		}
		catch(CursorException ex)
		{
			logger.error("Problema ricerca dn " + user.getUsername());
		}
		catch(LdapException ex)
		{
			logger.error("Problema ricerca dn " + user.getUsername());
		}
		
		// Preleva nome gruppo cui appartiene l'utente
		getUserGroup();
		if(!user.getGruppo().equals(""))
		{
			// Cerco DN gruppo
			try
			{
				String groupDn = "";
				try
				{												
					EntryCursor cursor = connection.search( dc, "(&(objectClass=groupOfNames)(member=" + user.getDn() +"))", SearchScope.SUBTREE, "*" );		
					while ( cursor.next() )
					{							
						Entry e = cursor.get();						
						Dn entryGroupDn = e.getDn();
						groupDn = entryGroupDn.getName();
					}			
				}
				catch(CursorException ex)
				{
					logger.error("Problema ricerca dn " + user.getUsername());
				}
				catch(LdapException ex)
				{
					logger.error("Problema ricerca dn " + user.getUsername());
				}
				if(!groupDn.equals(""))
				{
					Modification removedMember = new DefaultModification( ModificationOperation.REMOVE_ATTRIBUTE, "member", user.getDn() );				
					connection.modify( groupDn, removedMember );
					logger.info("Rimozione utente " + user.getUsername() + " da gruppo " + user.getGruppo() + " - by " + logger.getOperator().getUsername());
				}																
			}
			catch(LdapException ex)
			{
				logger.error("Problema di connessione LDAP");
			}
		}							
	}
	
	public void changeUserPassword()
	{
		try
		{			
			generateSha1();
			generateNTPassword();
			// Cerco DN utente
			try
			{												
				EntryCursor cursor = connection.search( "dc=irccsme,dc=it", "(&(objectClass=posixAccount)(uid=" + user.getUsername() +"))", SearchScope.SUBTREE, "cn" );		
				while ( cursor.next() )
				{							
					Entry e = cursor.get();
					user.setDn(e.get("cn").get().toString());					
				}			
			}
			catch(CursorException ex)
			{
				logger.error("Problema ricerca dn " + user.getUsername());
			}
			catch(LdapException ex)
			{
				logger.error("Problema ricerca dn " + user.getUsername());
			}
			
			Modification resetMemberPass = new DefaultModification( ModificationOperation.REPLACE_ATTRIBUTE, "userPassword", user.getSha1Password() );						
			connection.modify( user.getDn(), resetMemberPass );
			resetMemberPass = new DefaultModification( ModificationOperation.REPLACE_ATTRIBUTE, "sambaNTPassword", user.getNtPassword() );
			connection.modify( user.getDn(), resetMemberPass );
			logger.info("Password personale cambiata da utente " + user.getUsername());
		}
		catch(LdapException ex)
		{
			logger.error("Problema LDAP in cambio password utente");
		}
		catch(NoSuchAlgorithmException ex)
		{
			logger.error("Problema in cambio password utente");
		}
		catch(UnsupportedEncodingException ex)
		{
			logger.error("Problema in cambio password utente");
		}
	}
	
	public void resetUserPassword()
	{
		try
		{												
			EntryCursor cursor = connection.search( "dc=irccsme,dc=it", "(&(objectClass=posixAccount)(uid=" + user.getUsername() +"))", SearchScope.SUBTREE, "cn" );		
			while ( cursor.next() )
			{							
				Entry e = cursor.get();
				user.setDn(e.get("cn").get().toString());				
			}			
		}
		catch(CursorException ex)
		{
			logger.error("Problema reset password utente");
		}
		catch(LdapException ex)
		{
			logger.error("Problema reset password utente");
		}
		try
		{
			generateRandomPassword();
			generateSha1();
			generateNTPassword();
			Modification resetMemberPass = new DefaultModification( ModificationOperation.REPLACE_ATTRIBUTE, "userPassword", user.getSha1Password() );						
			connection.modify( user.getDn(), resetMemberPass );
			resetMemberPass = new DefaultModification( ModificationOperation.REPLACE_ATTRIBUTE, "sambaNTPassword", user.getNtPassword() );
			connection.modify( user.getDn(), resetMemberPass );
			logger.info("Reset password ad utente " + user.getUsername() + " - by " + logger.getOperator().getUsername());										
		}
		catch(LdapException ex)
		{
			logger.error("Reset password ad utente " + user.getUsername() + " fallito.");
		}
		catch(NoSuchAlgorithmException ex)
		{
			logger.error("Reset password ad utente " + user.getUsername() + " fallito.");
		}
		catch(UnsupportedEncodingException ex)
		{
			logger.error("Reset password ad utente " + user.getUsername() + " fallito.");
		}
	}
	
	public void generateRandomPassword() throws UnsupportedEncodingException, NoSuchAlgorithmException
	{				
		String randomString = RandomStringUtils.random(6, "abcdefghmnypqwz123456789");		
		user.setPlainPassword(randomString);
	}

	public void generateSha1() throws NoSuchAlgorithmException, UnsupportedEncodingException
	{				
		String sha1 = "{SHA}" + new sun.misc.BASE64Encoder()
				.encode(java.security.MessageDigest.getInstance("SHA1")
						.digest(user.getPlainPassword().getBytes()));				
		user.setSha1Password(sha1.toString());			
	}
	
	public void generateNTPassword() throws UnsupportedEncodingException 
	{        
        MD4 md4 = new MD4();
        byte[] bpass = user.getPlainPassword().getBytes("UnicodeLittleUnmarked");
        md4.engineUpdate(bpass, 0, bpass.length);        
        byte[] hashbytes = md4.engineDigest();               
        String ntHash = Hexdump.toHexString(hashbytes, 0, hashbytes.length * 2);          
        user.setNtPassword(ntHash);
    }	
}
